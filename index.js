http = require('http'),
path = require('path'),
express = require('express'),
app = express(),
server = http.createServer( app ),
rootFolder = path.resolve(__dirname),
bodyParser = require('./body2js'),
apiRoutes = require('./apiRoutes.js');

app.use( express.static( __dirname + '/page/' ) );

server.listen(3000);
