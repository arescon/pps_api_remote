/*
dbconfJson
		{
			"database":"infomat_dev",
			"user":"postgres",
			"password":"infomat123jelata",
			"port":5432,
			"ssl":true,
			"max":100,
			"min":5,
			"idleTimeoutMillis":10000
}
*/

module.exports = (fn, args, dbconfJson) => {
	return new Promise((resolve, reject) => {
		var params = '',
			values = [];
		args.map((el, i, arr) => {
			params += (i > 0 ? ',' : '') + el.name + ':=$' + (i + 1)
			values.push(el.value)
		})
		var Pool = require('pg-pool'),
			pool = new Pool(dbconfJson);
		pool.connect((err, pgclient, done) => {
			if (err) return reject(new Error('Не удалось подключитьсяк к базе'));
			pgclient.query(`select * from ${fn}(${params})`, values, (err, ress) => {
				pgclient.release();
				pool.end()
				
				if (err) return reject(err);
				resolve(ress)
			})
		})
	})
}