app.get('/dic/facs', ( request, response, next ) => {
	request.qc = {
        fn:'pps.fn_facs_sel',
        prms: []
    }
        
	request.clbk = (res)=>{ response.status(200).json( res.rows[0] )}
	next()
})
/* POST добавление */
app.post('/dic/fac', require('body2js')().json, ( request, response, next ) => {
	request.qc = {
    fn:'pps.fn_fac_ins',
    prms: [{
      name: 'injson',
      value: JSON.stringify({
        fullname: request.body.fullname,
        shortname: request.body.shortname,
        code: request.body.code,
      })
    }] }
	request.clbk = (res)=>{ response.status(200).json( { id: res.rows[0]._id })}
	next()
})
/*  PUT обновление  */
app.put('/dic/fac', require('body2js')().json, ( request, response, next ) => {
	request.qc = {
    fn:'pps.fn_fac_upd',
    prms: [{
      name: 'injson',
      value: JSON.stringify({
        id: request.query.id,
        fullname: request.body.fullname,
        shortname: request.body.shortname,
        code: request.body.code,
      })
    } ] }
	request.clbk = (res)=>{ response.status(200).json( { id: res.rows[0]._id })}
	next()
})
// app.delete( '/wbp/management', ( request, response, next ) => {
// 	request.qc = { fn:'dics.fn_managements_del', prms:[ { name: 'injson', value: JSON.stringify({
// 		user_pk: request.usr.id
// 		,id: request.query.id
// 	}) } ] }
// 	request.clbk = (res)=>{ response.status(200).json()}
// 	next()
// })

// app.put( '/wbp/management', /*require('body2js')().json,*/ ( request, response, next ) => {
// 	require('./uploader.js')(request, response, {path: '/var/statics/imgs/doctors/'}, (e,filename)=>{
// 		if(e) return response.status(500).json(e)
// 		let prms = {
// 			user_pk: request.usr.id
// 			,id: request.query.id
// 			,fam: request.body.fam
// 			,im: request.body.im
// 			,ot: request.body.ot
// 			,desc: request.body.desc
// 			,orgs_pk: request.body.orgs_pk
// 			,post_pk: request.body.post_pk
// 		}
// 		if (filename) prms.photo = 'http://api.infomat.jelata.tech/doctors/'+filename
// 		request.qc = { fn:'dics.fn_managements_upd', prms:[ { name: 'injson', value: JSON.stringify(prms) } ] }
// 		request.clbk = (res)=>{ response.status(200).json()}
// 		next()
// 	})
// })