app.get('/dic/kafedr', ( request, response, next ) => {
	request.qc = {
        fn:'pps.fn_kafedr_sel',
        prms: []
    }
        
	request.clbk = (res)=>{ response.status(200).json( res.rows[0] )}
	next();
})
/* POST добавление */
app.post('/dic/kafedr', require('body2js')().json, ( request, response, next ) => {
	request.qc = {
    fn:'pps.fn_kafedr_ins',
    prms: [{
      name: 'injson',
      value: JSON.stringify({
        name: request.body.name,
        fac_id: request.body.fac_id
      })
    } ] }
	request.clbk = (res)=>{ response.status(200).json( { id: res.rows[0]._id })}
	next();
})
app.put('/dic/kafedr', require('body2js')().json, ( request, response, next ) => {
	request.qc = {
    fn:'pps.fn_kafedr_upd',
    prms: [{
      name: 'injson',
      value: JSON.stringify({
        id: request.query.id,
        name: request.body.name,
        fac_id: request.body.fac_id
      })
    } ] }
	request.clbk = (res)=>{ response.status(200).json( { id: res.rows[0]._id })}
	next();
})