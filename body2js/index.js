module.exports = ( request, response, next ) => {
	return {
		body : (request, response, next)=>{
			var body=''
			request.on('data', (data)=>{
		  body += data;
		 })
		 request.on('end', ()=>{
		  request.body = {}
		  body.split('&').map((e,i,a)=>{var pair=e.split('=');request.body[pair[0]]=require('urlencode').decode(pair[1])})
				next()
		 });
		}
		,json : (request, response, next)=>{
			var body=''
			request.on('data', (data)=>{
		  body += data;
		 })
		 request.on('end', ()=>{
		  request.body = JSON.parse(body)
		  //body.split('&').map((e,i,a)=>{var pair=e.split('=');request.body[pair[0]]=require('urlencode').decode(pair[1])})
				next()
		 });
		}
	}
}

